// Create the Admin table
`CREATE TABLE Admins (
        AdminId INT AUTO_INCREMENT PRIMARY KEY,
        Username VARCHAR(255) NOT NULL UNIQUE,
        Password VARCHAR(255) NOT NULL,
        Email VARCHAR(255) NOT NULL UNIQUE,
        FullName VARCHAR(255) NOT NULL,
        Role VARCHAR(255) NOT NULL,
        Photo VARCHAR(255),
        PhoneNumber VARCHAR(100),
        City VARCHAR(100)
    );
    `

// Create the FleetManagers Table
`CREATE TABLE FleetManagers (
        FleetManagerId INT AUTO_INCREMENT PRIMARY KEY,
        Username VARCHAR(255) NOT NULL UNIQUE,
        Password VARCHAR(255) NOT NULL,
        Email VARCHAR(255) NOT NULL UNIQUE,
        FullName VARCHAR(255) NOT NULL,
        Role VARCHAR(255) NOT NULL,
        Photo VARCHAR(255),
        PhoneNumber VARCHAR(100),
        City VARCHAR(100)
    );
    `


