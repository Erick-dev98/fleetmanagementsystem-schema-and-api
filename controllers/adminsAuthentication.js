import { db } from "../connect.js";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";

export const register = (req,res)=>{

    // CHECK USER IF EXISTS
    const q = "SELECT * FROM admins WHERE Username = ?"
    
    db.query(q, [req.body.Username], (err,data)=>{
        if(err) return res.status(500).json(err);
        if(data.length) return res.status(409).json("User already exists!");

        //CREATE A NEW USER
        // Hash the password
        const salt = bcrypt.genSaltSync(10);
        const hashedPassword = bcrypt.hashSync(req.body.Password, salt)

        const q = "INSERT INTO admins (`Username`,`Password`,`Email`,`FullName`,`Role`) VALUE (?)";

        const values = [
            req.body.Username,
            hashedPassword,            
            req.body.Email,
            req.body.FullName,
            req.body.Role,
        ];

        db.query(q,[values], (err,data)=>{
            if(err) return res.status(500).json(err);
            return res.status(200).json("Admins user has been created...");
        });
    });

};


export const login = (req,res)=>{
    const q = "SELECT * FROM admins WHERE Username = ?"

    db.query(q, [req.body.Username], (err,data)=>{
        if(err) return res.status(500).json(err);
        if(data.length === 0) return res.status(404).json("User not found!")

        const checkPassword = bcrypt.compareSync(req.body.Password, data[0].Password)

        if(!checkPassword) return res.status(400).json("Wrong password or username!")

        const token = jwt.sign({id:data[0].id}, "secretkey");

        const {Password, ...others} = data[0]

        res.cookie("accessToken", token, {
            httpOnly: true,
        })
        .status(200)
        .json(others);
    });
};


export const logout = (req,res)=>{
    res.clearCookie("accessToken",{
        secure:true,
        sameSite:"none"
    }).status(200).json("User has been logged out.")
};


export const updateProfile = (req, res) => {
    const { AdminId, FullName, Photo, PhoneNumber, City } = req.body;
    // Construct the SQL UPDATE query
    const q = `
        UPDATE admins
        SET FullName = ?, Photo = ?, PhoneNumber = ?, City = ?
        WHERE AdminId = ?
    `;
    // Execute the query
    db.query(q, [FullName, Photo, PhoneNumber, City, AdminId], (err, result) => {
        if (err) {
            return res.status(500).json({ error: "Internal server error" });
        }
        if (result.affectedRows === 0) {
            return res.status(404).json({ error: "User not found" });
        }
        return res.status(200).json({ message: "User profile updated successfully" });
    });
};

