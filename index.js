import express from "express";
import cors from 'cors';
import { db } from "./connect.js";

const app = express();

// Use CORS middleware
app.use(cors({ origin: 'https://fleetmanagenmenttool-frontend.vercel.app' }));

// API request to client
app.get("/", (req, res) => {
    res.json("Hello this is the backend");
});

// Connect to MySQL database
db.connect((err) => {
    if (err) {
        throw err;
    }
    console.log('MySQL Connected...');
});

// Create middleware to parse JSON bodies
app.use(express.json());

// Endpoint to handle registration
app.post('/register', (req, res) => {
    const { name, username, password } = req.body;
    const sql = "INSERT INTO signup (`name`, `username`, `password`) VALUES (?, ?, ?)";
    const values = [name, username, password];
    db.query(sql, values, (err, result) => {
        if (err) {
            console.error("Error inserting record:", err);
            return res.status(500).json({ error: "Error inserting record" });
        }
        console.log("Record inserted successfully:", result);
        return res.json({ success: true });
    });
});

// Endpoint to handle Login
app.post('/login', (req, res) => {
    const sql = "SELECT * FROM signup WHERE `username` = ? AND `password` = ?";
    db.query(sql, [req.body.username, req.body.password], (err, data) => {
        if (err) {
            console.error("Error executing database query:", err);
            return res.status(500).json({ error: "Error executing database query" });
        }
        console.log("Query result:", data);
        if (data.length > 0) {
            console.log("Login successful for username:", req.body.username);
            return res.json({ success: true, redirect: "/home" });
        } else {
            console.log("No record found for username:", req.body.username);
            return res.json({ success: false, message: "No record existed" });
        }
    });
});


// Start the server
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
    console.log(`API running on port ${PORT}`);
});

export default app;
