import express from "express";
import { login, register, logout, updateProfile } from "../controllers/fleetManagersAuthentication.js";

const router = express.Router()

router.post("/login", login)
router.post("/register", register)
router.post("/logout", logout)
router.post("/updateProfile", updateProfile)

export default router